<?php
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
session_start();
require 'vendor/autoload.php';
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$config['db']['host']   = "localhost";
$config['db']['user']   = "root";
$config['db']['pass']   = "";
$config['db']['dbname'] = "jomarisk8er";

$app = new \Slim\App(["settings" => $config]);
$container = $app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer("templates");


// $app->get("/",function($req, $res){
	// return $res->getBody()->write("Homepage");
// });

$app->get('/', function ($request, $response) {
    return $response->getBody()->write('Hello World');
});

$app->group('/utils', function () use ($app) {
    $app->get('/date', function ($request, $response) {
        return $response->getBody()->write(date('Y-m-d H:i:s'));
    });
    $app->get('/time', function ($request, $response) {
        return $response->getBody()->write(time());
    });

})->add(function ($request, $response, $next) {
    $response->getBody()->write('It is now ');
    $response = $next($request, $response);
    $response->getBody()->write('. Enjoy!');

    return $response;
});

$container['db'] = function($c){
	$db = $c['settings']['db'];
	$mysqli = new mysqli($db['host'],$db['user'],$db['pass'],$db['dbname']);
	return $mysqli;
};

// $app->get('/tickets', function ( $request,  $response) {
	
	// $tickets = 
			// [
				// [
					// "id" => 1,
					// "title" => "title",
					// "component" => "component",
					// "desc" => "description"
				// ],
				// [
					// "id" => 2,
					// "title" => "title2",
					// "component" => "component2",
					// "desc" => "description2"
				// ]
			// ];
	// $response = $this->view->render($response, "tickets.phtml", ["tickets" => $tickets, "router" => $this->router]);
	// return $response;
// });

$app->get('/ticket/{id}', function ( $request, $response, $args) {
    echo "Ticket id is " , $args['id'];
})->setName("ticket-detail");

$app->get('/hello/{name}', function ($request, $response, $args) {
    echo "Hello, " . $args['name'];
});

$app->run();